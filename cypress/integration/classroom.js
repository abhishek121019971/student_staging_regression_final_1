describe('HomeComponent', function ()
{
    //polyfill window.fetch from tests
    let polyfill
    before(() => {
      const polyfillUrl = 'https://unpkg.com/unfetch/dist/unfetch.umd.js'
    cy.request(polyfillUrl)
      .then((response) => {
        polyfill = response.body
      })
    })

    beforeEach(function () {
    cy.visit('http://student-staging.lidolearning.com/', {
        onBeforeLoad (win) {
          delete win.fetch
          win.eval(polyfill)
          win.fetch = win.unfetch
        },
      })
      cy.get('#mobile').type(2222222222)
      cy.get('.bottom > .btn').should('be.visible').should('not.be.disabled').click()
      cy.url().should('include','/login/otp')
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(130012)
      
      cy.server()
      cy.fixture('/home/homefixture_3.json').as('comment')
      cy.route('POST','/graphql','@comment').as('response')
      cy.route('POST','/create-ably-token',{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IncxNy1idy5mT1laM0EifQ.eyJ4LWFibHktY2FwYWJpbGl0eSI6IntcInN0dWRlbnQ6OWRlNjk0NWQtYmY5MS00M2QzLThjMTctZGEwMTJjNGVkYjczXCI6W1wic3Vic2NyaWJlXCJdLFwidmM6KlwiOltcInN1YnNjcmliZVwiLFwicHVibGlzaFwiXX0iLCJpYXQiOjE1ODQ4Nzc4ODIsImV4cCI6MTU4NDg4MTQ4Mn0.dthIOAC_m2WDY1wfTJSlFkDPBnigMtGcjTRuW-J3qTs"}).as('ably')
      cy.get(':nth-child(3) > .btn').should('not.be.disabled').click() 
      cy.wait('@response')
      //cy.wait('@ably')
    })

    it('Course_Selector',function(){
        cy.server()
          cy.fixture('classroom/classroomfixture_2.json').as('comm')
          cy.route('POST','/graphql','@comm').as('res')
          cy.contains('Classroom').click({force:true})    
          cy.wait('@res')
            //cy.get('.course-selector-label').should('have.text','Course Selector:')
            //upclasses heading etc should be aded here
          cy.get(':nth-child(2) > .mt-5 > .font-21').should('have.text','Upcoming Class')
          cy.get(':nth-child(3) > .mt-5 > .font-21').should('have.text','Past Classes')

     })

    it('No_Upcoming Class', function()
    {
        cy.server()
        cy.fixture('classroom/classroomfixture_1.json').as('comm')
        cy.route('POST','/graphql','@comm').as('res')
        cy.contains('Classroom').click({force:true})    
        cy.wait('@res')
        cy.contains('Looks like you have no upcoming classes.')
        cy.contains('Contact your academic advisor for your next class.') 
    })

    it('One_Upcoming_Class', function()
    {
      cy.server()
      cy.fixture('classroom/classroomfixture_2.json').as('comm')
      cy.route('POST','/graphql','@comm').as('res')
      cy.contains('Classroom').click({force:true})    
      cy.wait('@res')
      cy.get('.align-items-center.w-100 > .row > .col-md-11').should('not.be.empty')
      cy.get('.align-items-center.w-100 > .row > .col-md-1').should('not.be.empty')
      cy.get('.col-md-1 > .font-21').should('have.text','L2')
      cy.get('.mr-4 > .flex-grow-1 > .font500').should('have.text','Thu, 2 April')
      cy.get('.mr-4 > .flex-grow-1 > :nth-child(2)').should('have.text','10:30am to 11:30am')
      cy.get(':nth-child(2) > .d-flex > .flex-grow-1 > .font500').should('have.text','Evolution')
      cy.get(':nth-child(2) > .d-flex > .flex-grow-1 > :nth-child(2)').should('have.text','Lido Exploration Camp')
      cy.get('.col-md-11 > :nth-child(2) > .btn').should('have.text','Join class 🌈').should('be.disabled')
    })

    it('One_Upcoming_Class', function()
    {
      cy.server()
      cy.fixture('classroom/classroomfixture_3.json').as('comm')
      cy.route('POST','/graphql','@comm').as('res')
      cy.contains('Classroom').click({force:true})    
      cy.wait('@res')
      cy.get('.align-items-center.w-100 > .row > .col-md-11').should('not.be.empty')
      cy.get('.align-items-center.w-100 > .row > .col-md-1').should('not.be.empty')
      cy.get('.col-md-1 > .font-21').should('have.text','L2')
      cy.get('.mr-4 > .flex-grow-1 > .font500').should('have.text','Thu, 2 April')
      cy.get('.mr-4 > .flex-grow-1 > :nth-child(2)').should('have.text','10:30am to 11:30am')
      cy.get(':nth-child(2) > .d-flex > .flex-grow-1 > .font500').should('have.text','Evolution')
      cy.get(':nth-child(2) > .d-flex > .flex-grow-1 > :nth-child(2)').should('have.text','Lido Exploration Camp')
      cy.get('.col-md-11 > :nth-child(2) > .btn').should('have.text','Join class 🌈').should('not.be.disabled')
      cy.contains('Join class 🌈').click({force:true})
      cy.url().should('include','vc.lidolearning.com')
    })

    it('No_Past_classes',function(){
        cy.server()
        cy.fixture('classroom/classroomfixture_4.json').as('comm')
        cy.route('POST','/graphql','@comm').as('res')
        cy.contains('Classroom').click({force:true})    
        cy.wait('@res')
        cy.contains('Your past classes will appear here.')

    })

    it('One_Past_classes',function(){
        cy.server()
        cy.fixture('classroom/classroomfixture_5.json').as('comm')
        cy.route('POST','/graphql','@comm').as('res')
        cy.contains('Classroom').click({force:true})    
        cy.wait('@res')
        cy.get('.p-0 > :nth-child(1) > .col-md-1').should('not.be.empty')
        cy.get('.p-0 > :nth-child(1) > .col-md-11 > .row').should('not.be.empty')
        cy.get('.p-0 > :nth-child(1) > .col-md-1 > .font-21').should('have.text','C1')
        cy.get('.w-100 > :nth-child(1) > .font-14').should('have.text','1 Lesson')   
        cy.get('.p-0 > :nth-child(1) > .col-md-11 > .row > .w-100 > :nth-child(1) > .font-18').should('have.text','Lido Exploration Camp')
        cy.get('.w-100 > :nth-child(1) > .font-14').should('have.text','1 Lesson')
        cy.get(':nth-child(2) > .flex-grow-1 > .font-14').should('have.text','All Challenges Completed')
    })

    it('No_Homework',function(){
        cy.server()
        cy.fixture('classroom/classroomfixture_5.json').as('comm')
        cy.route('POST','/graphql','@comm').as('res')
        cy.contains('Classroom').click({force:true})    
        cy.wait('@res')
        cy.get('.p-0 > :nth-child(1) > .col-md-11 > .row').click({force:true})
        cy.get('.pl-3 > div > .w-100').should('have.text','Homework Challenge: No homework !')

    })

    it('No_Homework',function(){
        cy.server()
        cy.fixture('classroom/classroomfixture_5.json').as('comm')
        cy.route('POST','/graphql','@comm').as('res')
        cy.contains('Classroom').click({force:true})    
        cy.wait('@res')
        cy.get('.completed-unit > .col-md-1').should('not.be.empty')
        cy.get('.completed-unit > .col-md-1 > .d-block').should('have.text','L2')
        cy.get('.p-0 > :nth-child(1) > .col-md-11 > .row').click({force:true})
        cy.get('.row > .mb-0').should('have.text','Evolution')
        cy.get('.pl-3 > div > .w-100').should('have.text','Homework Challenge: No homework !')
        cy.get('.underline').should('have.text','Revise').click({force:true})
        cy.url().should('include','/library/chapter')
    })

    it('Skipped_Homework',function(){
        cy.server()
        cy.fixture('classroom/classroomfixture_6.json').as('comm')
        cy.route('POST','/graphql','@comm').as('res')
        cy.contains('Classroom').click({force:true})    
        cy.wait('@res')
        cy.get('.completed-unit > .col-md-1').should('not.be.empty')
        cy.get('.completed-unit > .col-md-1 > .d-block').should('have.text','L11')
        cy.get('.p-0 > :nth-child(1) > .col-md-11 > .row').click({force:true})
        cy.get('.row > .mb-0').should('have.text','Understanding Friction')
        cy.get('.pl-3 > div > .w-100').should('have.text','Homework Challenge: Skipped')
        cy.get('.underline').should('have.text','Revise').click({force:true})
        cy.url().should('include','/library/chapter')
    })

    it('Two_Lessons_of_A_Chapter',function()
    {
      cy.server()
      cy.fixture('classroom/classroomfixture_7.json').as('comm')
      cy.route('POST','/graphql','@comm').as('res')
      cy.contains('Classroom').click({force:true})    
      cy.wait('@res')
      cy.get('.p-0 > :nth-child(1) > .col-md-1').should('not.be.empty')
      cy.get('.p-0 > :nth-child(1) > .col-md-11 > .row').should('not.be.empty')
      cy.get('.p-0 > :nth-child(1) > .col-md-1 > .font-21').should('have.text','C1')
      cy.get('.p-0 > :nth-child(1) > .col-md-11 > .row > .w-100 > :nth-child(1) > .font-18').should('have.text','Friction')
      cy.get('.w-100 > :nth-child(1) > .font-14').should('have.text','2 Lessons')
      cy.get(':nth-child(2) > .flex-grow-1 > .font-14').should('have.text','2 Challenges Pending')
      cy.get('.completed-units > :nth-child(1) > .col-md-1').should('not.be.empty')
      cy.get('.completed-units > :nth-child(1) > .col-md-1 > .d-block').should('have.text','L12')
      cy.get(':nth-child(1) > .col-md-11 > .bg-grey > .d-flex.justify-content-between > .d-flex > .row > .mb-0').should('have.text','Understanding Friction')
      cy.get(':nth-child(1) > .col-md-11 > .bg-grey > .pl-3 > div > .w-100').should('have.text','Homework Challenge: Skipped')
      cy.get(':nth-child(1) > .col-md-11 > .bg-grey > .d-flex.justify-content-between > .d-flex > .row > .underline').should('have.text','Revise')
      cy.get(':nth-child(2) > .col-md-1').should('not.be.empty')
      cy.get(':nth-child(2) > .col-md-11 > .bg-grey > .d-flex.justify-content-between > .d-flex > .row > .mb-0').should('have.text','Understanding Friction')
      cy.get(':nth-child(2) > .col-md-11 > .bg-grey > .pl-3 > div > .w-100').should('have.text','Homework Challenge: Skipped')
      cy.get(':nth-child(2) > .col-md-11 > .bg-grey > .d-flex.justify-content-between > .d-flex > .row > .underline').should('have.text','Revise')
    })


    it('Two_Lessons_of_A_Chapter',function()
    {
      cy.server()
      cy.fixture('classroom/classroomfixture_8.json').as('comm')
      cy.route('POST','/graphql','@comm').as('res')
      cy.contains('Classroom').click({force:true})    
      cy.wait('@res')
      cy.get(':nth-child(2) > .p-0 > :nth-child(1) > .col-md-11 > .row').should('not.be.empty')
      cy.get(':nth-child(2) > .p-0 > :nth-child(1) > .col-md-1').should('not.be.empty')
      cy.get(':nth-child(2) > .p-0 > :nth-child(1) > .col-md-1 > .font-21').should('have.text','C1')
      cy.get(':nth-child(2) > .p-0 > :nth-child(1) > .col-md-11 > .row > .w-100 > :nth-child(1) > .font-18').should('have.text','Friction')
      cy.get(':nth-child(2) > .p-0 > :nth-child(1) > .col-md-11 > .row > .w-100 > :nth-child(1) > .font-14').should('have.text','1 Lesson')
      cy.get(':nth-child(2) > .p-0 > :nth-child(1) > .col-md-11 > .row > .w-100 > :nth-child(2) > .flex-grow-1 > .font-14').should('have.text','1 Challenge Pending')
      cy.get(':nth-child(2) > .p-0 > :nth-child(1) > .col-md-11 > .row').click({force:true})
      cy.get(':nth-child(2) > .p-0 > .collapse > .completed-units > .completed-unit > .col-md-11 > .bg-grey').should('not.be.empty')
      cy.get(':nth-child(2) > .p-0 > .collapse > .completed-units > .completed-unit > .col-md-1').should('not.be.empty')
      cy.get(':nth-child(2) > .p-0 > .collapse > .completed-units > .completed-unit > .col-md-1 > .d-block').should('have.text','L11')
      cy.get(':nth-child(2) > .p-0 > .collapse > .completed-units > .completed-unit > .col-md-11 > .bg-grey > .d-flex.justify-content-between > .d-flex > .row > .mb-0').should('have.text','Understanding Friction')
      cy.get('.pl-3 > div > .w-100').should('have.text','Homework Challenge: Skipped')
      cy.get(':nth-child(2) > .p-0 > .collapse > .completed-units > .completed-unit > .col-md-11 > .bg-grey > .d-flex.justify-content-between > .d-flex > .row > .underline').should('have.text','Revise')
    })
    it('Two_Lessons_of_A_Chapter',function()
    {
      cy.server()
      cy.fixture('classroom/classroomfixture_9.json').as('comm')
      cy.route('POST','/graphql','@comm').as('res')
      cy.contains('Classroom').click({force:true})    
      cy.wait('@res')
      
      cy.get(':nth-child(2) > .p-0 > :nth-child(1) > .col-md-11 > .row').should('not.be.empty')
      cy.get(':nth-child(1) > .p-0 > :nth-child(1) > .col-md-11 > .row').should('not.be.empty')
      cy.get(':nth-child(1) > .p-0 > :nth-child(1) > .col-md-1 > .font-21').should('have.text','C1')
      cy.get(':nth-child(1) > .p-0 > :nth-child(1) > .col-md-11 > .row > .w-100 > :nth-child(1) > .font-18').should('have.text','Friction')
      cy.get(':nth-child(1) > .p-0 > :nth-child(1) > .col-md-11 > .row > .w-100 > :nth-child(1) > .font-14').should('have.text','1 Lesson')
      cy.get(':nth-child(1) > .p-0 > :nth-child(1) > .col-md-11 > .row > .w-100 > :nth-child(2) > .flex-grow-1 > .font-14').should('have.text','1 Challenge Pending')
      cy.get(':nth-child(1) > .p-0 > :nth-child(1) > .col-md-11 > .row').click({force:true})
      cy.get(':nth-child(1) > .p-0 > .collapse > .completed-units > .completed-unit > .col-md-1').should('not.be.empty')
      cy.get(':nth-child(1) > .p-0 > .collapse > .completed-units > .completed-unit > .col-md-11 > .bg-grey').should('not.be.empty')
      cy.get(':nth-child(1) > .p-0 > .collapse > .completed-units > .completed-unit > .col-md-1 > .d-block').should('have.text','L12')
      //cy.get('.d-flex.justify-content-between > .d-flex > .row > .mb-0').should('have.text',"Understanding Frict ion new")
      //cy.get('.pl-3 > .row > :nth-child(1)').should('have.text','Homework Challenge: Due in 7 Days')
      //cy.get('.pl-3 > .row > :nth-child(2)').should('have.text',"Gems to earn0")
      cy.get('.underline').should('have.text','Revise')
      cy.get('.row > .btn').should('have.text','Launch Challenge').should('not.be.disabled').click({force:true})
      })
//when home work is pending
 //check for when haapens for the text of all challenges completed.  
})

//need to optimize it with the before hook or jwt tokwn /or ably token (saving) to avoid create state
describe('Lido_Logo_Functionality',function()
{
    //login need to be put in a class
    beforeEach(function ()
    {
        cy.visit('http://student-staging.lidolearning.com/')
        cy.get('#mobile').type(2222222223)
        cy.get('.bottom > .btn').should('be.visible').should('not.be.disabled').click()
        cy.url().should('include','/login/otp')
        cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(130012)

        cy.server()
        cy.fixture('/home/homefixture_3.json').as('comment')
        cy.route('POST','/graphql','@comment').as('response')
        cy.route('POST','/create-ably-token',{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IncxNy1idy5mT1laM0EifQ.eyJ4LWFibHktY2FwYWJpbGl0eSI6IntcInN0dWRlbnQ6OWRlNjk0NWQtYmY5MS00M2QzLThjMTctZGEwMTJjNGVkYjczXCI6W1wic3Vic2NyaWJlXCJdLFwidmM6KlwiOltcInN1YnNjcmliZVwiLFwicHVibGlzaFwiXX0iLCJpYXQiOjE1ODQ4Nzc4ODIsImV4cCI6MTU4NDg4MTQ4Mn0.dthIOAC_m2WDY1wfTJSlFkDPBnigMtGcjTRuW-J3qTs"}).as('ably')
        cy.get(':nth-child(3) > .btn').should('not.be.disabled').click()
        
    })

    it('Lido_Button_Functionality',function()
    {
        cy.get('.navbar-brand > .img-fluid').click({force:true})  
        cy.url().should('include','student-staging.lidolearning.com/')
    })

    it('Correct_rewards points are rendered intially',function()
    {
        cy.get('.m-auto > :nth-child(4) > .nav-link').click({force:true})  
        cy.url().should('include','/rewards/rewards-store')
        cy.contains('My Rewards').click({force:true})
        cy.contains('Total gems earned: 175')
        cy.get('.container > :nth-child(1) > .font-21').should('have.text','Working Towards')
        cy.get('.col-md-2').should('not.be.empty')
        cy.get('.font-24 > .d-flex').should('have.text','3000')
        cy.get('.col-md-7').should('not.be.empty')
        cy.get('.ml-3').should('have.text',"Amazon Gift Voucher")
        cy.get('.col-md-3').should('not.be.empty')
        cy.get('.col-md-3 > .font-18').should('have.text','13 days')
    })

    it('Profile_Functionality',function()
    {
        cy.get('.nav-avtar > .img-fluid').click({force:true})
        cy.get('.arrow_box > :nth-child(1)').should('have.text','Edit Profile').click({force:true})
        cy.get('.my-3.d-flex').should('not.be.empty')
        cy.get(':nth-child(3) > :nth-child(1) > .ml-2').should('have.text','First name*')
        cy.get('#firstName').clear().type("Sambodhi")

        cy.get(':nth-child(3) > :nth-child(2) > .ml-2').should('have.text','Last name*')
        cy.get('#lastName').clear().type("Goyal")

        cy.get(':nth-child(4) > :nth-child(2) > .ml-2').should('have.text','Gender*')
        cy.get(':nth-child(4) > :nth-child(2) > .form-control').select('Female')
        

        cy.get('.my-3.col > .ml-2').should('have.text','Email')
        cy.get('#email').clear().type('nehagoyal2483@gmail.com')

        cy.get('.custom > .ml-2').should('have.text','Date of Birth*')
        cy.get('.react-datepicker__input-container > .form-control').click()
        //date of birth not taken into account
        
        cy.contains('Save Changes').click({force:true})
        cy.get('.text-center > .font-18').should('have.text','Hi Sambodhi!')

    })
    
    it('Test my system_Functionality',function()
    {
        cy.get('.nav-avtar > .img-fluid').click({force:true})
        cy.get('.arrow_box > :nth-child(3)').should('have.text','Test my system').click({force:true})
        cy.url().should('include','https://vc.lidolearning.com/static/system-check?role=student')
    })

    it('Feedback_Functionality',function()
    {
        cy.get('.nav-avtar > .img-fluid').click({force:true})
        cy.contains('Give feedback').click({force:true})
        cy.url().should('include','/feedback/general')
        cy.get('.feedbackback > .img-fluid').click({force:true})
        cy.url().should('include','student-staging.lidolearning.com/')
        cy.get('.nav-avtar > .img-fluid').click({force:true})
        cy.contains('Give feedback').click({force:true})
        //cy.url().should('include','/feedback/general')
        cy.get('.questionnaire-header').should('have.text','Your opinions are valuable to us, and will be used to improve your Lido experience.')
        cy.get('.question-subtitle').should('have.text','Please let us know if you have any feedback or concerns.')
        cy.get('.feedback_textarea').type("this is a feedback ",{force:true})
        cy.contains('Submit').click({force:true})
        cy.get('.headpoppins').should('have.text','Thank you for your valuable feedback!Introducing Rewards!')
        cy.get('.font-18').should('have.text','We will try to take this into account to improve your LIDO experience.')
        cy.get('.col-12 > .btn').click({force:true})
        cy.url().should('include','student-staging.lidolearning.com/')
    })

    it('Logout_Functionality',function()
    {
        cy.get('.nav-avtar > .img-fluid').click({force:true})
        cy.contains('Logout').click({force:true})
    })
})

describe('HomeComponent', function ()
{
    //polyfill window.fetch from tests
    let polyfill
    before(() => {
      const polyfillUrl = 'https://unpkg.com/unfetch/dist/unfetch.umd.js'
    cy.request(polyfillUrl)
      .then((response) => {
        polyfill = response.body
      })
    })

    beforeEach(function () {
    cy.visit('http://student-staging.lidolearning.com/', {
        onBeforeLoad (win) 
        {
          delete win.fetch
          win.eval(polyfill)
          win.fetch = win.unfetch
        },
      })
      cy.get('#mobile').type(2222222223)
      cy.get('.bottom > .btn').should('be.visible').should('not.be.disabled').click()
      cy.url().should('include','/login/otp')
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(130012)
    })

    it('No_Classes,Chalenges,Homework', function()
    {
      cy.server()
      cy.fixture('home/homefixture_1.json').as('comment')
      cy.route('POST','/graphql','@comment').as('response')
      cy.route('POST','/create-ably-token',{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IncxNy1idy5mT1laM0EifQ.eyJ4LWFibHktY2FwYWJpbGl0eSI6IntcInN0dWRlbnQ6MTg5NTVlNmEtNmVlYS00OGY1LWEwZDMtNTE2ZmI2OWUxMWY0XCI6W1wic3Vic2NyaWJlXCJdLFwidmM6KlwiOltcInN1YnNjcmliZVwiLFwicHVibGlzaFwiXX0iLCJpYXQiOjE1ODUzNDA2NjMsImV4cCI6MTU4NTM0NDI2M30.tzRZDi_DGl4ixlVHabhkel1_SNrtsif5pXcYbpRuc20"}).as('ably')
      cy.get(':nth-child(3) > .btn').should('not.be.disabled').click()
      cy.wait('@response')
      //cy.wait('@ably') 
      //check for the image.
      cy.get('h2').should('have.text',"Looks like you have no upcoming classes ☹️")
      cy.get('.font-24').should('have.text',"Call your academic advisor for your next class. In the meantime, check out our content library")
      cy.get('.underline').click({force:true})
      cy.url().should('include','student-staging.lidolearning.com/')
    })

     it('One_Upcoming_Class', function()
     {
       cy.server()
       cy.fixture('home/homefixture_2.json').as('comment')
       cy.route('POST','/graphql','@comment').as('response')
       cy.route('POST','/create-ably-token',{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IncxNy1idy5mT1laM0EifQ.eyJ4LWFibHktY2FwYWJpbGl0eSI6IntcInN0dWRlbnQ6MTg5NTVlNmEtNmVlYS00OGY1LWEwZDMtNTE2ZmI2OWUxMWY0XCI6W1wic3Vic2NyaWJlXCJdLFwidmM6KlwiOltcInN1YnNjcmliZVwiLFwicHVibGlzaFwiXX0iLCJpYXQiOjE1ODUzNDA2NjMsImV4cCI6MTU4NTM0NDI2M30.tzRZDi_DGl4ixlVHabhkel1_SNrtsif5pXcYbpRuc20"}).as('ably')
       cy.get(':nth-child(3) > .btn').should('not.be.disabled').click()
       cy.wait('@response')
       //cy.wait('@ably') 
       
       cy.get('.text-center > .font-18').should('have.text',"Hi Satakshi !")

       cy.get('h2').should('have.text',"Here are your next classes")
       
       cy.get('.card-body').should('be.visible').should('not.be.disabled').should('not.be.empty')
       
       cy.get('.py-3').should('have.text',"Science")
       
       cy.get(':nth-child(1) > .col-md-9 > .font500').should('have.text',"Thu, 2 April")
       cy.get(':nth-child(1) > .col-md-9 > :nth-child(2)').should('have.text',"10:30am to 11:30am")

       cy.get(':nth-child(2) > .col-md-9 > .font500').should('have.text',"Evolution")
       cy.get(':nth-child(2) > .col-md-9 > :nth-child(2)').should('have.text',"Lido Exploration Camp")

       cy.get(':nth-child(3) > .col-md-9 > .font500').should('have.text',"Aniket Batabyal")

       cy.contains('Join class').should('be.visible').should('be.disabled')
    })

    it('Two_Upcoming_Class', function()
    {
      cy.server()
       cy.fixture('home/homefixture_3.json').as('comment')
       cy.route('POST','/graphql','@comment').as('response')
       cy.route('POST','/create-ably-token',{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IncxNy1idy5mT1laM0EifQ.eyJ4LWFibHktY2FwYWJpbGl0eSI6IntcInN0dWRlbnQ6MTg5NTVlNmEtNmVlYS00OGY1LWEwZDMtNTE2ZmI2OWUxMWY0XCI6W1wic3Vic2NyaWJlXCJdLFwidmM6KlwiOltcInN1YnNjcmliZVwiLFwicHVibGlzaFwiXX0iLCJpYXQiOjE1ODUzNDA2NjMsImV4cCI6MTU4NTM0NDI2M30.tzRZDi_DGl4ixlVHabhkel1_SNrtsif5pXcYbpRuc20"}).as('ably')
       cy.get(':nth-child(3) > .btn').should('not.be.disabled').click()
       cy.wait('@response')
       //cy.wait('@ably') 
       
      cy.get(':nth-child(1) > .card-body > .py-3').should('not.be.disabled').should('not.be.empty')
      cy.get(':nth-child(2) > .card-body > .py-3').should('not.be.disabled').should('not.be.empty')  
    })

    // //need to test wheather class is active works or not
    it('One_Upcoming_Class_Which_Is _Active', function()
    {
       cy.server()
       cy.fixture('home/homefixture_4.json').as('comment')
       cy.route('POST','/graphql','@comment').as('response')
       cy.route('POST','/create-ably-token',{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IncxNy1idy5mT1laM0EifQ.eyJ4LWFibHktY2FwYWJpbGl0eSI6IntcInN0dWRlbnQ6MTg5NTVlNmEtNmVlYS00OGY1LWEwZDMtNTE2ZmI2OWUxMWY0XCI6W1wic3Vic2NyaWJlXCJdLFwidmM6KlwiOltcInN1YnNjcmliZVwiLFwicHVibGlzaFwiXX0iLCJpYXQiOjE1ODUzNDA2NjMsImV4cCI6MTU4NTM0NDI2M30.tzRZDi_DGl4ixlVHabhkel1_SNrtsif5pXcYbpRuc20"}).as('ably')
       cy.get(':nth-child(3) > .btn').should('not.be.disabled').click()
       cy.wait('@response')
       //cy.wait('@ably') 
       
       cy.get('.text-center > .font-18').should('have.text',"Hi Satakshi !")

       cy.get('h2').should('have.text',"Here are your next classes")
       
       cy.get('.card-body').should('be.visible').should('not.be.disabled').should('not.be.empty')
       
       cy.get('.py-3').should('have.text',"Science")
       
       cy.get(':nth-child(1) > .col-md-9 > .font500').should('have.text',"Thu, 2 April")
       cy.get(':nth-child(1) > .col-md-9 > :nth-child(2)').should('have.text',"10:30am to 11:30am")

       cy.get(':nth-child(2) > .col-md-9 > .font500').should('have.text',"Evolution")
       cy.get(':nth-child(2) > .col-md-9 > :nth-child(2)').should('have.text',"Lido Exploration Camp")

       cy.get(':nth-child(3) > .col-md-9 > .font500').should('have.text',"Aniket Batabyal")

       cy.contains('Join class').should('be.visible').should('not.be.disabled')
       cy.get('.text-center > .btn').click({force:true})
       cy.url().should('include','//vc.lidolearning.com')
    })
    //homework chalenges are still need to be testsed
})

describe('HomeComponent', function ()
{
    //polyfill window.fetch from tests
    let polyfill
    before(() => {
      const polyfillUrl = 'https://unpkg.com/unfetch/dist/unfetch.umd.js'
    cy.request(polyfillUrl)
      .then((response) => {
        polyfill = response.body
      })
    })

    beforeEach(function () {
    cy.visit('http://student-staging.lidolearning.com/', {
        onBeforeLoad (win) {
          delete win.fetch
          win.eval(polyfill)
          win.fetch = win.unfetch
        },
      })
      cy.get('#mobile').type(2222222222)
      cy.get('.bottom > .btn').should('be.visible').should('not.be.disabled').click()
      cy.url().should('include','/login/otp')
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(130012)
      
      cy.server()
      cy.fixture('/home/homefixture_3.json').as('comment')
      cy.route('POST','/graphql','@comment').as('response')
      cy.route('POST','/create-ably-token',{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IncxNy1idy5mT1laM0EifQ.eyJ4LWFibHktY2FwYWJpbGl0eSI6IntcInN0dWRlbnQ6OWRlNjk0NWQtYmY5MS00M2QzLThjMTctZGEwMTJjNGVkYjczXCI6W1wic3Vic2NyaWJlXCJdLFwidmM6KlwiOltcInN1YnNjcmliZVwiLFwicHVibGlzaFwiXX0iLCJpYXQiOjE1ODQ4Nzc4ODIsImV4cCI6MTU4NDg4MTQ4Mn0.dthIOAC_m2WDY1wfTJSlFkDPBnigMtGcjTRuW-J3qTs"}).as('ably')
      cy.get(':nth-child(3) > .btn').should('not.be.disabled').click() 
      cy.wait('@response')
      //cy.wait('@ably')
      cy.server()
      cy.fixture('/library/libraryfixture_1.json').as('comm')
      cy.route('POST','/graphql','@comm').as('res')
      cy.contains('Library').click({force:true})   
      
    })

    //check the url
    
    // it('One_chapter_In_library', function()
    // {

    //   //cy.wait('@res')
    //   cy.get('.number-indicator').should('not.be.empty')
    //   cy.get('.number-indicator > span').should('have.text','1')
    //   cy.get('.card-body > .my-3').should('have.text','Lido Exploration Camp')
    //   cy.server()
    //   cy.fixture('/library/libraryfixture_1.json').as('comm')
    //   cy.route('POST','/graphql',).as('res')
    //   cy.get('.col-sm-6 > .nopadding').should('not.be.empty')
    // })

    // it('One_chapter_In_library', function()
    // {
    //   cy.server()
    //   cy.fixture('/library/libraryfixture_1.json').as('comm')
    //   cy.route('POST','/graphql','@comm').as('res')
    //   cy.contains('Library').click({force:true})    
    //   cy.server()
    //   cy.fixture('/library/libraryfixture_1.1.json').as('fix')
    //   cy.route('POST','/graphql','@fix').as('res')
    //   cy.get('.col-sm-6 > .nopadding').should('not.be.empty').click({force:true})
    //   cy.get('.p-3 > .font-18').should('have.text','Lido Exploration Camp')
    //   cy.get('#content-1-tab > .row > .col-9').should('have.text','Activities')
    //   cy.get('#content-2-tab > .row > .col-9').should('have.text','Videos')
    //   cy.get('#content-4-tab > .row > .col-9').should('have.text','Quizzes')
    // })

    // it('Back_Button_functionality', function()
    // {
    //   cy.server()
    //   cy.fixture('/library/libraryfixture_1.json').as('comm')
    //   cy.route('POST','/graphql','@comm').as('res')
    //   cy.contains('Library').click({force:true})    
    //   cy.server()
    //   cy.fixture('/library/libraryfixture_1.1.json').as('fix')
    //   cy.route('POST','/graphql','@fix').as('res')
    //   cy.get('.col-sm-6 > .nopadding').should('not.be.empty').click({force:true})
    //   cy.contains(' Back')
    //   cy.get('.back').click({force:true})
    //   cy.url().should('include','/library')
     
    // })

    // it('Activities_functionality', function()
    // {
    //   cy.server()
    //   cy.fixture('/library/libraryfixture_1.json').as('comm')
    //   cy.route('POST','/graphql','@comm').as('res')
    //   cy.contains('Library').click({force:true})    
    //   cy.server()
    //   cy.fixture('/library/libraryfixture_1.1.json').as('fix')
    //   cy.route('POST','/graphql','@fix').as('res')
    //   cy.get('.col-sm-6 > .nopadding').should('not.be.empty').click({force:true})
    //   cy.get('#content-1-tab > .row > .col-9').should('have.text','Activities').click({force:true})
    //   cy.get('#content-1 > .container > .my-5 > .d-flex > .library-card > .card-body > .font-18').should('have.text','Microorganisms and Medicines')
    //   cy.get('#content-1 > .container > .my-5 > :nth-child(1) > .library-card > .card-body > .font-18').click({force:true})
    //   cy.get("main-window-slide-container cs-slide-container").should('not.be.empty')
    //   cy.get('span').click({force:true})
    //   //not able check the blue tick.
    //   //so you check for image wheather presenet or not.
    //   //cy.get('.tickBox > .img-fluid').should('')
    // })

    // it('Video_functionality', function()
    // {
    //   cy.server()
    //   cy.fixture('/library/libraryfixture_1.json').as('comm')
    //   cy.route('POST','/graphql','@comm').as('res')
    //   cy.contains('Library').click({force:true})    
    //   cy.server()
    //   cy.fixture('/library/libraryfixture_1.1.json').as('fix')
    //   cy.route('POST','/graphql','@fix').as('res')
    //   cy.get('.col-sm-6 > .nopadding').should('not.be.empty').click({force:true})
    //   cy.get('#content-1-tab > .row > .col-9').should('have.text','Activities').click({force:true})
    //   cy.get('#content-1 > .container > .my-5 > .d-flex > .library-card > .card-body > .font-18').should('have.text','Microorganisms and Medicines')
    //   cy.get('#content-1 > .container > .my-5 > :nth-child(1) > .library-card > .card-body > .font-18').click({force:true})
    //   cy.get("main-window-slide-container cs-slide-container").should('not.be.empty')
    //   cy.get('span').click({force:true})
    //   //not able check the blue tick.
    //   //so you check for image wheather presenet or not.
    //   //cy.get('.tickBox > .img-fluid').should('')
    // })

    it('Quiz_functionality', function()
    {
      cy.server()
      cy.fixture('/library/libraryfixture_1.json').as('comm')
      cy.route('POST','/graphql','@comm').as('res')
      cy.contains('Library').click({force:true})    
      cy.server()
      cy.fixture('/library/libraryfixture_1.1.json').as('fix')
      cy.route('POST','/graphql','@fix').as('res')
      cy.get('.col-sm-6 > .nopadding').should('not.be.empty').click({force:true})
      cy.get('#content-1-tab > .row > .col-9').should('have.text','Activities').click({force:true})
      cy.get('#content-1 > .container > .my-5 > .d-flex > .library-card > .card-body > .font-18').should('have.text','Microorganisms and Medicines')
      cy.get('#content-1 > .container > .my-5 > :nth-child(1) > .library-card > .card-body > .font-18').click({force:true})
      cy.get("main-window-slide-container cs-slide-container").should('not.be.empty')
      
      cy.server()
      cy.fixture('/library/libraryfixture_1.1.json').as('fix')
      cy.route('POST','/graphql','@fix').as('res')
      cy.route('POST','/graphql', {"data":{"update_activity_track":{"id":"9f642db8-79fb-4f1d-ab67-34fb0fe90e74","__typename":"Track"}}})
      
      cy.get('span').click({force:true})
     
      //not able check the blue tick.
      //so you check for image wheather presenet or not.
      //cy.get('.tickBox > .img-fluid').should('')
    })



  })


  describe('Login_Page_Check',function()
  {
      //The BACKGRAOUND IMAGES TEST CASES ARE NOT TAKEN INTO ACCOUNT  
      //CSS AND STYLING TEST CASES ALSO NOT TAKEN INTO ACCOUNT
      it('Visit_Url',()=>{
      //need to put base url there 
          cy.visit('http://student-staging.lidolearning.com/')
      })

      it('Check_Route',()=>{
          cy.url().should('include','login/mobile')
      })

      it('Check_Login_Message_First',()=>{
          cy.contains('Hey 👋').should('be.visible')
      })

      it('Check_Login_Message_Second',()=>{
          cy.contains('Ready to log in?').should('be.visible')
      })

      it('Login_Header',()=>{
          cy.contains('Enter registered mobile number')
      })
      //CHECK THE BACKGROUND SHOULD HAVE TEXT "enter registered mobile no."
      it('CheckInputBox',()=>{
          cy.get('input[type="number"]').should('be.visible').should('not.be.disabled').should('be.empty')
      })

      it('Check_Continue_Button',()=>{
          cy.contains('Continue').should('be.disabled')
      })
  })

describe('Login_Tests',function()
  {
      //need to use the base url.
      beforeEach(()=>{
          cy.visit('http://student-staging.lidolearning.com/')
      })
      //we can also create a object and use it for fetching the static data.
      var testCaseArray = ["",28888888,1145342675,'HJKHKJHKJHKJHKHJKH',2222222222];
      var testCaseDataNumber=1;
      
      it('Requires_Mobile_Number',function(){
          cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty')
          cy.contains('Continue').should('be.disabled')
      })
  
      it('Mobile_Number_Size_Lessthan_10',function(){
          cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(testCaseArray[testCaseDataNumber++])
          cy.contains('Continue').should('be.disabled')
      })
  
      it('Mobile_Number_invalid',function(){
          cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(testCaseArray[testCaseDataNumber++])
          cy.contains('Continue').should('not.be.disabled').click()
          cy.contains('Please provide a valid phone number.').should('be.visible')
      })
      
      it('Mobile_Number_Is_Number',function(){
          cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(testCaseArray[testCaseDataNumber++])
          cy.contains('Continue').should('be.disabled')
      })
  
      it('Mobile_Number_Is_Registetred',function(){
          cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(testCaseArray[testCaseDataNumber++])
          cy.contains('Continue').should('be.visible').should('not.be.disabled').click()
          cy.url().should('include','login/otp')
      })
  })

//NEED TO OPTIMIZE WITH THE ABBLY TOKEN REPONSE SAVING
//NOT ABLE TO CHECK FOR RESEND OTP FUNCTIONALITY
describe('Otp_Page_Check',function()
  {
      it('Visit_Url',function(){
          cy.visit('http://student-staging.lidolearning.com/')
          cy.get('#mobile').type(2222222222)
          cy.get('.bottom > .btn').should('be.visible').should('not.be.disabled').click()
          cy.url().should('include','/login/otp')
      })

      it('Otp_Heading',()=>{
          cy.contains('We’ve sent an OTP to your mobile number').should('be.visible')
      })
      
      it('Otp_Header',()=>{
          cy.contains('Enter OTP').should('be.visible')
      })

      it('CheckInputBox',()=>{
          cy.get('input[type="number"]').should('be.visible').should('not.be.disabled').should('be.empty')
      })

      it('Check_Continue_Button',()=>{
          cy.contains('Continue').should('be.disabled')
      })
      //not able to check the functionaality of the otp.
      it('Check_Resend_OTP_Button',()=>{
          cy.contains('Resend OTP').should('be.visible').should('not.be.disabled')
      })
  })
//NEED TO OPTIMIZE WITH THE ABBLY TOKEN REPONSE SAVING

describe('Otp_Tests',function()
{
  beforeEach(()=>
  {
      cy.visit('http://student-staging.lidolearning.com/')
      cy.get('#mobile').type(2222222222)
      cy.get('.bottom > .btn').should('be.visible').should('not.be.disabled').click()
      cy.url().should('include','/login/otp')
  })
  var testCaseArray = ["",78996,676588,'jkjkjhkj',130012];
  var testCaseDataNumber=1;
     
  it('Otp_Required',function(){
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty')
      cy.contains('Continue').should('be.visible').should('be.disabled')
  })

  it('Otp_Size_Lessthan_6',function(){
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(testCaseArray[testCaseDataNumber++])
      cy.contains('Continue').should('be.visible').should('be.disabled')
  })
  
  it('Otp_invalid',function(){
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(testCaseArray[testCaseDataNumber++])
      cy.contains('Continue').should('be.visible').should('not.be.disabled').click()
      cy.contains('Please provide a valid OTP.').should('be.visible')
  })

  it('Otp_Is_Number',function(){
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(testCaseArray[testCaseDataNumber++])
      cy.contains('Continue').should('be.visible').should('be.disabled')
  })
  
  it('Otp_Is_Registered',function(){
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(testCaseArray[testCaseDataNumber++])
      cy.contains('Continue').should('not.be.disabled').should('be.visible').click()
      cy.url().should('include','https://student-staging.lidolearning.com/')
  })
})



describe('HomeComponent', function ()
{
    //polyfill window.fetch from tests
    let polyfill
    before(() => {
      const polyfillUrl = 'https://unpkg.com/unfetch/dist/unfetch.umd.js'
    cy.request(polyfillUrl)
      .then((response) => {
        polyfill = response.body
      })
    })

    beforeEach(function () {
    cy.visit('http://student-staging.lidolearning.com/', {
        onBeforeLoad (win) {
          delete win.fetch
          win.eval(polyfill)
          win.fetch = win.unfetch
        },
      })
      cy.get('#mobile').type(2222222222)
      cy.get('.bottom > .btn').should('be.visible').should('not.be.disabled').click()
      cy.url().should('include','/login/otp')
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(130012)
      cy.server()
      cy.route('POST','/graphql',{"data":{"profile":{"first_name":"Satakshi ","last_name":"Dixit","date_of_birth":"2009-01-09T18:30:00+00:00","gender":"female","email":"nehagoyal2483@gmail.com","student":{"avatar":{"id":"3","name":"Avatar 3","image_url":"https://d39460vivz6red.cloudfront.net/avatars/avatar-3.svg","thumbnail":"https://d39460vivz6red.cloudfront.net/avatars/avatar-thumb-3.svg","__typename":"Avatar"},"__typename":"Student"},"__typename":"User"},"courses":[{"slug":"BDA_SALES_1","subject":{"id":"1","name":"Maths","__typename":"Subject"},"__typename":"Course"},{"slug":"SC_CBSE_8","subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},{"slug":"M_CBSE_7","subject":{"id":"1","name":"Maths","__typename":"Subject"},"__typename":"Course"},{"slug":"SC_CBSE_7","subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},{"slug":"ENG_FEL_2","subject":{"id":"3","name":"English","__typename":"Subject"},"__typename":"Course"},{"slug":"M_CBSE_8","subject":{"id":"1","name":"Maths","__typename":"Subject"},"__typename":"Course"},{"slug":"SC_CBSE_9","subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"}],"all_courses":{"completed_courses":[{"title":"Science CBSE Grade 8","grade":{"id":"8","__typename":"Grade"},"slug":"SC_CBSE_8","subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},{"title":"Math CBSE Grade 7","grade":{"id":"7","__typename":"Grade"},"slug":"M_CBSE_7","subject":{"id":"1","name":"Maths","__typename":"Subject"},"__typename":"Course"},{"title":"Science CBSE Grade 7","grade":{"id":"7","__typename":"Grade"},"slug":"SC_CBSE_7","subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},{"title":"English FEL 2","grade":{"id":"5","__typename":"Grade"},"slug":"ENG_FEL_2","subject":{"id":"3","name":"English","__typename":"Subject"},"__typename":"Course"}],"current_courses":[{"title":"BDA Sales Training","grade":{"id":"8","__typename":"Grade"},"slug":"BDA_SALES_1","subject":{"id":"1","name":"Maths","__typename":"Subject"},"__typename":"Course"},{"title":"Science CBSE Grade 8","grade":{"id":"8","__typename":"Grade"},"slug":"SC_CBSE_8","subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},{"title":"Science CBSE Grade 9","grade":{"id":"9","__typename":"Grade"},"slug":"SC_CBSE_9","subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},{"title":"Math CBSE Grade 8","grade":{"id":"8","__typename":"Grade"},"slug":"M_CBSE_8","subject":{"id":"1","name":"Maths","__typename":"Subject"},"__typename":"Course"}],"future_courses":[],"__typename":"AllCourses"},"all_subjects":[{"id":"1","name":"Maths","__typename":"Subject"},{"id":"2","name":"Science","__typename":"Subject"},{"id":"3","name":"English","__typename":"Subject"},{"id":"4","name":"Sales","__typename":"Subject"}],"rewards":{"total_points_earned":78,"total_points_redeemed":0,"__typename":"EarnedRewardPoints"},"current_goal":{"id":"6","points":5000,"name":"Amazon Gift Voucher","level_type":3,"expires_in_ms":"1146825378","__typename":"CurrentGoal"},"feedbacks":[{"title":"Hope you enjoyed class today!","subtitle":"Tell us what you thought","id":"cd1e632a-ba28-45be-9aa1-c70f9dd7600f","is_commentable":false,"feedback_type":"class_completion","questions":[{"options":[{"others":false,"choices":[],"ratings":[2,1,3],"is_multiple_choice":false}],"id":"663f5c85-216e-45ba-9eb2-2895c61a2d62","rating_type":"emoji","subtitle":"","title":"How did you feel about this class?","__typename":"FeedbackQuestion"},{"options":[{"others":false,"choices":[],"ratings":[1],"is_multiple_choice":false},{"others":true,"choices":[{"tag":"Camera_issue","title":"Camera had issues"},{"tag":"Sound_issue","title":"Sound had issues"},{"tag":"Video_issue","title":"Video did not load"}],"ratings":[2],"is_multiple_choice":true}],"id":"91e64abf-e68c-4f27-bc20-d7fa35169923","rating_type":"yes_no","subtitle":"","title":"Did you face technical issues?","__typename":"FeedbackQuestion"},{"options":[{"others":false,"choices":[],"ratings":[5],"is_multiple_choice":false},{"others":true,"choices":[{"tag":"Too_fast","title":"Tutor should speak slower"},{"tag":"Doubtsolving_issue","title":"Tutor should be stronger at solving doubts"},{"tag":"Participation_issue","title":"I wanted more opportunity to participate"},{"tag":"Not_interesting","title":"Should have been more interesting"},{"tag":"Explanation_issue","title":"Should have explained things better"}],"ratings":[4,3,2,1],"is_multiple_choice":true}],"id":"e6205e6a-b9e7-48b9-a2a9-c15684033efb","rating_type":"star","subtitle":"","title":"How was your tutor?","__typename":"FeedbackQuestion"},{"options":[{"others":false,"choices":[],"ratings":[3],"is_multiple_choice":false},{"others":true,"choices":[],"ratings":[2,1],"is_multiple_choice":false}],"id":"c153311b-1d58-4e03-bdb9-b04ce03129cb","rating_type":"emoji","subtitle":"","title":"How do you feel about attending your next class?","__typename":"FeedbackQuestion"},{"options":[{"others":false,"choices":[],"ratings":[],"is_multiple_choice":false}],"id":"2cb69098-b643-4ee7-81c1-843a5cda71ba","rating_type":"generic","subtitle":"","title":"Any other comments/feedback?","__typename":"FeedbackQuestion"}],"__typename":"FeedbackQuestionnaire"},{"title":"Congrats on completing this chapter. We hope you enjoyed it!","subtitle":"Tell us what you thought","id":"75b92b2f-0331-4439-919c-b8fec2bda70b","is_commentable":false,"feedback_type":"chapter_completion","questions":[{"options":[{"others":true,"choices":[{"tag":"NO_LEARNING","title":"I could not learn much in this chapter"},{"tag":"BAD_CONTENT","title":"The content in the classes was not good"},{"tag":"BAD_TUTOR","title":"The tutor in this chapter was not effective"},{"tag":"TECH_DIFFICULTIES","title":"I faced technical difficulties during this chapter"},{"tag":"NOT_RELEVANT","title":"The curriculum in this chapter was not relevant to me"},{"tag":"TOO_DIFFICULT","title":"It was too difficult for me to understand"}],"ratings":[2,1],"is_multiple_choice":true},{"others":false,"choices":[],"ratings":[3],"is_multiple_choice":false}],"id":"fc4afb32-4b7d-4e7b-9eff-5ef6539965ec","rating_type":"emoji","subtitle":"","title":"What was your overall feeling about the live classes in this chapter?","__typename":"FeedbackQuestion"},{"options":[{"others":true,"choices":[{"tag":"TOO_QUICK","title":"Speaks too quickly"},{"tag":"NO_SOLVE_DOUBTS","title":"Not able to solve my doubts"},{"tag":"NO_CLASS_PARTICIPATION","title":"Did not give enough opportunity to participate in class"},{"tag":"NO_EXPLANATION","title":"Was not good at explaining things"},{"tag":"BORING","title":"Was boring"}],"ratings":[2,1],"is_multiple_choice":true},{"others":false,"choices":[],"ratings":[3],"is_multiple_choice":false}],"id":"2b89d163-f0d6-49f2-8d42-7aa101250529","rating_type":"emoji","subtitle":"","title":"What was your overall feeling about your tutor throughout this chapter?","__typename":"FeedbackQuestion"},{"options":[{"others":true,"choices":[{"tag":"Not_Learn","title":"I did not learn much from this chapter"},{"tag":"Not_Enjoy","title":"I did not enjoy this chapter"},{"tag":"Content_Bad","title":"I did not find the content good"},{"tag":"Tutor_Ineffective","title":"The tutor was not effective"},{"tag":"Bored","title":"I got bored during this chapter"},{"tag":"HW_Not_Complete","title":"I could not complete the Homework Challenges for this chapter"},{"tag":"HW_Ineffective","title":"The Homework Challenges did not help my learning"}],"ratings":[2,1],"is_multiple_choice":true},{"others":false,"choices":[],"ratings":[3],"is_multiple_choice":false}],"id":"5b5acf45-d44e-477d-854a-1996d8b6cc36","rating_type":"emoji","subtitle":"","title":"How was your overall learning experience in this chapter?","__typename":"FeedbackQuestion"}],"__typename":"FeedbackQuestionnaire"},{"title":"Your opinions are valuable to us, and will be used to improve your Lido experience.","subtitle":"Please let us know if you have any feedback or concerns.","id":"c2712c90-f823-4606-90ee-0b7d2b910733","is_commentable":true,"feedback_type":"general","questions":[],"__typename":"FeedbackQuestionnaire"},{"title":"Rate this content!","subtitle":"Tell us what you thought","id":"26a7b02f-a229-4761-bd6a-f6d298447f40","is_commentable":false,"feedback_type":"content_completion","questions":[{"options":[{"others":true,"choices":[{"tag":"Understanding_Difficult","title":"It was difficult to understand"},{"tag":"Already_Knew","title":"I already knew the material in it"},{"tag":"Not_Useful","title":"It didn't contain helpful information"},{"tag":"Slow","title":"It was boring or too slow"},{"tag":"Not_Understand_Person","title":"I could not hear or understand the person talking"}],"ratings":[3,2,1],"is_multiple_choice":true},{"others":false,"choices":[],"ratings":[4,5],"is_multiple_choice":false}],"id":"c964e2ea-2359-42fa-a8d6-829e2d1cc924","rating_type":"star","subtitle":"","title":"What was your overall feeling about the content?","__typename":"FeedbackQuestion"}],"__typename":"FeedbackQuestionnaire"}],"completed":[{"unit":{"slug":"ECSC_CBSE_9_U_1_3","course_units":[{"course":{"subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},"__typename":"CourseUnit"}],"content":[],"__typename":"Unit"},"__typename":"ClassCompleted"},{"unit":{"slug":"ENG_FEL_2_U_1_1","course_units":[{"course":{"subject":{"id":"3","name":"English","__typename":"Subject"},"__typename":"Course"},"__typename":"CourseUnit"}],"content":[],"__typename":"Unit"},"__typename":"ClassCompleted"}],"upcoming":[{"start_time":"2020-04-02T05:00:00+00:00","end_time":"2020-04-02T06:00:00+00:00","time_left":"234225358","vc_id":null,"unit":{"slug":"ECSC_CBSE_9_U_1_3","course_units":[{"course":{"subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},"__typename":"CourseUnit"}],"name":"Evolution","chapter":{"name":"Lido Exploration Camp","__typename":"Chapter"},"__typename":"Unit"},"teacher":{"first_name":"Aniket","last_name":"Batabyal","__typename":"User"},"__typename":"VirtualClass"},{"start_time":"2020-04-02T06:00:00+00:00","end_time":"2020-04-02T07:00:00+00:00","time_left":"237825352","vc_id":null,"unit":{"slug":"ECSC_CBSE_9_U_1_4","course_units":[{"course":{"subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},"__typename":"CourseUnit"}],"name":"The Last Supper","chapter":{"name":"Lido Exploration Camp","__typename":"Chapter"},"__typename":"Unit"},"teacher":{"first_name":"Aniket","last_name":"Batabyal","__typename":"User"},"__typename":"VirtualClass"}],"homeworkChallenges":[{"id":"d921e40b-237e-4fa5-988e-8428c3a63a20","total_points":0,"is_streak_mode":false,"end_time_in_ms":0,"content":[],"unit":{"slug":"ECSC_CBSE_9_U_1_3","course_units":[{"course":{"subject":{"id":"2","name":"Science","__typename":"Subject"},"__typename":"Course"},"__typename":"CourseUnit"}],"__typename":"Unit"},"__typename":"Homework"}],"last_completed_chapter":[{"chapter":{"id":"04a1d69f-a20a-46bf-a3b3-0ec70131fc23","thumbnail":"https://content-thumbnails.s3.amazonaws.com/chapters/Personal Correspondence/1582982345258.png","title":"Personal Correspondence","__typename":"Chapter"},"completed_at":"2020-03-02T14:45:00+00:00","__typename":"ChapterCompleted"}],"testHomeworks":[]}}).as('response')
      cy.route('POST','/create-ably-token',{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IncxNy1idy5mT1laM0EifQ.eyJ4LWFibHktY2FwYWJpbGl0eSI6IntcInN0dWRlbnQ6OWRlNjk0NWQtYmY5MS00M2QzLThjMTctZGEwMTJjNGVkYjczXCI6W1wic3Vic2NyaWJlXCJdLFwidmM6KlwiOltcInN1YnNjcmliZVwiLFwicHVibGlzaFwiXX0iLCJpYXQiOjE1ODQ4Nzc4ODIsImV4cCI6MTU4NDg4MTQ4Mn0.dthIOAC_m2WDY1wfTJSlFkDPBnigMtGcjTRuW-J3qTs"}).as('ably')
      cy.get(':nth-child(3) > .btn').should('not.be.disabled').click() 
      cy.wait('@response')
      //cy.wait('@ably')
    })

    it('Performance_when_no classes_has_been_done', function()
    {
        cy.server()
        cy.route('POST','/graphql',{"data":{"get_chapter_reports":[]}}).as('response')
        cy.get('.m-auto > :nth-child(5) > .nav-link').click({force:true})  
        cy.wait('@response')
        cy.get('h2').should('have.text','Performance will show up after you finish your first chapter🤓')
        cy.get('.font-24').should('have.text','Check back then!')
    })

    it('Performance_Needs improvement', function()
    {
        cy.server()
        cy.route('POST','/graphql',{"data":{"get_chapter_reports":[{"score":0.3083333333333333,"chapter":{"id":"0849060b-4dc7-48ae-bff9-da4c42eb4eb3","title":"Friction","__typename":"Chapter"},"__typename":"ChapterScore"}]}}).as('response')
        cy.get('.m-auto > :nth-child(5) > .nav-link').click({force:true})  
        cy.wait('@response')
        cy.contains('Your overall performance:')
        cy.get('.font-24 > :nth-child(2)').should('have.text','Needs improvement 🙁')
        cy.get(':nth-child(1) > .ml-3 > .headpoppins').should('have.text','Needs improvement')
        cy.get(':nth-child(1) > .ml-3 > .colorgrey').should('have.text','Revise the core concepts')

        cy.get(':nth-child(2) > .ml-3 > .headpoppins').should('have.text','Doing okay')
        cy.get(':nth-child(2) > .ml-3 > .colorgrey').should('have.text','Practice to improve your scores')

        cy.get(':nth-child(3) > .ml-3 > .headpoppins').should('have.text','Doing great')
        cy.get(':nth-child(3) > .ml-3 > .colorgrey').should('have.text','Test your skills through games')
        cy.get(':nth-child(3) > .d-flex').should('not.be.empty')
        cy.get(':nth-child(1) > .mb-0').should('have.text','Friction')
        cy.contains('Revise').click({force:true})
        //now you can check wheather routes to the correct page or not.
    })

    it('Performance_Doing okay', function()
    {
        cy.server()
        cy.route('POST','/graphql',{"data":{"get_chapter_reports":[]}}).as('response')
        cy.get('.m-auto > :nth-child(5) > .nav-link').click({force:true})  
        cy.wait('@response')
        cy.get('h2').should('have.text','Performance will show up after you finish your first chapter🤓')
        cy.get('.font-24').should('have.text','Check back then!')
        cy.server()
        cy.route('POST','/graphql',{"data":{"get_chapter_reports":[{"score":0.5083333333333333,"chapter":{"id":"0849060b-4dc7-48ae-bff9-da4c42eb4eb3","title":"Friction","__typename":"Chapter"},"__typename":"ChapterScore"}]}}).as('response')
        cy.get('.m-auto > :nth-child(5) > .nav-link').click({force:true})  
        cy.wait('@response')
        cy.contains('Your overall performance:')
        cy.get('.font-24 > :nth-child(2)').should('have.text','Doing Okay 🙂')
        cy.get(':nth-child(1) > .ml-3 > .headpoppins').should('have.text','Needs improvement')
        cy.get(':nth-child(1) > .ml-3 > .colorgrey').should('have.text','Revise the core concepts')

        cy.get(':nth-child(2) > .ml-3 > .headpoppins').should('have.text','Doing okay')
        cy.get(':nth-child(2) > .ml-3 > .colorgrey').should('have.text','Practice to improve your scores')

        cy.get(':nth-child(3) > .ml-3 > .headpoppins').should('have.text','Doing great')
        cy.get(':nth-child(3) > .ml-3 > .colorgrey').should('have.text','Test your skills through games')
        cy.get(':nth-child(3) > .d-flex').should('not.be.empty')
        cy.get(':nth-child(1) > .mb-0').should('have.text','Friction')
        cy.contains('Revise').click({force:true})

        //now you can check wheather routes to the correct page or not.
    })

    it('Performance_Doing great', function()
    {
      cy.server()
      cy.route('POST','/graphql',{"data":{"get_chapter_reports":[{"score":0.7083333333333333,"chapter":{"id":"0849060b-4dc7-48ae-bff9-da4c42eb4eb3","title":"Friction","__typename":"Chapter"},"__typename":"ChapterScore"}]}}).as('response')
      cy.get('.m-auto > :nth-child(5) > .nav-link').click({force:true})  
      cy.wait('@response')
      cy.contains('Your overall performance:')
      cy.get('.font-24 > :nth-child(2)').should('have.text','Doing Great 😎')
      cy.get(':nth-child(1) > .ml-3 > .headpoppins').should('have.text','Needs improvement')
      cy.get(':nth-child(1) > .ml-3 > .colorgrey').should('have.text','Revise the core concepts')

      cy.get(':nth-child(2) > .ml-3 > .headpoppins').should('have.text','Doing okay')
      cy.get(':nth-child(2) > .ml-3 > .colorgrey').should('have.text','Practice to improve your scores')

      cy.get(':nth-child(3) > .ml-3 > .headpoppins').should('have.text','Doing great')
      cy.get(':nth-child(3) > .ml-3 > .colorgrey').should('have.text','Test your skills through games')
      cy.get(':nth-child(3) > .d-flex').should('not.be.empty')
      cy.get(':nth-child(1) > .mb-0').should('have.text','Friction')
      cy.contains('Revise').click({force:true})

      //now you can check wheather routes to the correct page or not.
    })
    //to check wheatherthe averages are compuuted coretly or not.
    it('when_two_classes_are_done_and_', function()
    {
      cy.server()
      cy.route('POST','/graphql',{"data":{"get_chapter_reports":[{"score":0.3083333333333333,"chapter":{"id":"0849060b-4d jjc7-48ae-bff9-da4c42eb4eb3","title":"Friction","__typename":"Chapter"},"__typename":"ChapterScore"},{"score":0.0883333333333333,"chapter":{"id":"0849060b-4dc7-48ae-bff9-da4c42eb4eb3","title":"Friction","__typename":"Chapter"},"__typename":"ChapterScore"}]}}).as('response')
      cy.get('.m-auto > :nth-child(5) > .nav-link').click({force:true})  
      cy.wait('@response')
      cy.contains('Your overall performance:')
      cy.get('.font-24 > :nth-child(2)').should('have.text','Needs improvement 🙁')
      cy.get(':nth-child(1) > .ml-3 > .headpoppins').should('have.text','Needs improvement')
      cy.get(':nth-child(1) > .ml-3 > .colorgrey').should('have.text','Revise the core concepts')

      cy.get(':nth-child(2) > .ml-3 > .headpoppins').should('have.text','Doing okay')
      cy.get(':nth-child(2) > .ml-3 > .colorgrey').should('have.text','Practice to improve your scores')

      cy.get(':nth-child(3) > .ml-3 > .headpoppins').should('have.text','Doing great')
      cy.get(':nth-child(3) > .ml-3 > .colorgrey').should('have.text','Test your skills through games')
      // cy.get(':nth-child(3) > .d-flex').should('not.be.empty')
      // cy.get(':nth-child(1) > .mb-0').should('have.text','Friction')
      // cy.contains('Revise').click({force:true})

      //now you can check wheather routes to the correct page or not.
    })

    it('when_two_classes_are_done_and_', function()
    {
      cy.server()
      cy.route('POST','/graphql',{"data":{"get_chapter_reports":[{"score":0.3083333333333333,"chapter":{"id":"0849060b-4d jjc7-48ae-bff9-da4c42eb4eb3","title":"Friction","__typename":"Chapter"},"__typename":"ChapterScore"},{"score":0.5883333333333333,"chapter":{"id":"0849060b-4dc7-48ae-bff9-da4c42eb4eb3","title":"Friction","__typename":"Chapter"},"__typename":"ChapterScore"}]}}).as('response')
      cy.get('.m-auto > :nth-child(5) > .nav-link').click({force:true})  
      cy.wait('@response')
      cy.contains('Your overall performance:')
      cy.get('.font-24 > :nth-child(2)').should('have.text','Doing Okay 🙂')
      cy.get(':nth-child(1) > .ml-3 > .headpoppins').should('have.text','Needs improvement')
      cy.get(':nth-child(1) > .ml-3 > .colorgrey').should('have.text','Revise the core concepts')

      cy.get(':nth-child(2) > .ml-3 > .headpoppins').should('have.text','Doing okay')
      cy.get(':nth-child(2) > .ml-3 > .colorgrey').should('have.text','Practice to improve your scores')

      cy.get(':nth-child(3) > .ml-3 > .headpoppins').should('have.text','Doing great')
      cy.get(':nth-child(3) > .ml-3 > .colorgrey').should('have.text','Test your skills through games')
      // cy.get(':nth-child(3) > .d-flex').should('not.be.empty')
      // cy.get(':nth-child(1) > .mb-0').should('have.text','Friction')
      // cy.contains('Revise').click({force:true})

      //now you can check wheather routes to the correct page or not.
    })

    it('when_two_classes_are_done_and_', function()
    {
      cy.server()
      cy.route('POST','/graphql',{"data":{"get_chapter_reports":[{"score":0.7083333333333333,"chapter":{"id":"0849060b-4d jjc7-48ae-bff9-da4c42eb4eb3","title":"Friction","__typename":"Chapter"},"__typename":"ChapterScore"},{"score":0.8883333333333333,"chapter":{"id":"0849060b-4dc7-48ae-bff9-da4c42eb4eb3","title":"Friction","__typename":"Chapter"},"__typename":"ChapterScore"}]}}).as('response')
      cy.get('.m-auto > :nth-child(5) > .nav-link').click({force:true})  
      cy.wait('@response')
      cy.contains('Your overall performance:')
      cy.get('.font-24 > :nth-child(2)').should('have.text','Doing Great 😎')
      cy.get(':nth-child(1) > .ml-3 > .headpoppins').should('have.text','Needs improvement')
      cy.get(':nth-child(1) > .ml-3 > .colorgrey').should('have.text','Revise the core concepts')

      cy.get(':nth-child(2) > .ml-3 > .headpoppins').should('have.text','Doing okay')
      cy.get(':nth-child(2) > .ml-3 > .colorgrey').should('have.text','Practice to improve your scores')

      cy.get(':nth-child(3) > .ml-3 > .headpoppins').should('have.text','Doing great')
      cy.get(':nth-child(3) > .ml-3 > .colorgrey').should('have.text','Test your skills through games')
      // cy.get(':nth-child(3) > .d-flex').should('not.be.empty')
      // cy.get(':nth-child(1) > .mb-0').should('have.text','Friction')
      // cy.contains('Revise').click({force:true})

      //now you can check wheather routes to the correct page or not.
    })


  })
 


  describe('HomeComponent', function ()
{
    //polyfill window.fetch from tests
    let polyfill
    before(() => {
      const polyfillUrl = 'https://unpkg.com/unfetch/dist/unfetch.umd.js'
    cy.request(polyfillUrl)
      .then((response) => {
        polyfill = response.body
      })
    })

    beforeEach(function () {
    cy.visit('http://student-staging.lidolearning.com/', {
        onBeforeLoad (win) {
          delete win.fetch
          win.eval(polyfill)
          win.fetch = win.unfetch
        },
      })
      cy.get('#mobile').type(2222222222)
      cy.get('.bottom > .btn').should('be.visible').should('not.be.disabled').click()
      cy.url().should('include','/login/otp')
      cy.get('#mobile').should('be.visible').should('not.be.disabled').should('be.empty').type(130012)
      
      cy.server()
      cy.fixture('/home/homefixture_3.json').as('comment')
      cy.route('POST','/graphql','@comment').as('response')
      cy.route('POST','/create-ably-token',{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IncxNy1idy5mT1laM0EifQ.eyJ4LWFibHktY2FwYWJpbGl0eSI6IntcInN0dWRlbnQ6OWRlNjk0NWQtYmY5MS00M2QzLThjMTctZGEwMTJjNGVkYjczXCI6W1wic3Vic2NyaWJlXCJdLFwidmM6KlwiOltcInN1YnNjcmliZVwiLFwicHVibGlzaFwiXX0iLCJpYXQiOjE1ODQ4Nzc4ODIsImV4cCI6MTU4NDg4MTQ4Mn0.dthIOAC_m2WDY1wfTJSlFkDPBnigMtGcjTRuW-J3qTs"})
      cy.get(':nth-child(3) > .btn').should('not.be.disabled').click({force:true}) 
    })


    it('Expired_Rewards', function()
    {
        cy.server()
        //cy.fixture('/home/homefixture_3.json').as('comment')
        cy.contains('Rewards').click({force:true})
        //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"7","name":"Baskin & Robbins Voucher","desc":"A voucher worth Rs. 350 to be spent at any Baskin & Robbins outlet on ice cream.","type":"Food","points":2000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":1,"__typename":"Reward"},{"id":"8","name":"PVR Cinema Voucher","desc":"A voucher worth Rs. 500 which can be availed at any PVR Cinema.","type":"Entertainment","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":2,"__typename":"Reward"}]}}).as('res1')
        cy.route('POST','/graphql',{"data":{"rewards":[{"id":"7","name":"Baskin & Robbins Voucher","desc":"A voucher worth Rs. 350 to be spent at any Baskin & Robbins outlet on ice cream.","type":"Food","points":2000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":1,"__typename":"Reward"},{"id":"8","name":"PVR Cinema Voucher","desc":"A voucher worth Rs. 500 which can be availed at any PVR Cinema.","type":"Entertainment","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":2,"__typename":"Reward"}]}})
        cy.contains('Rewards').click({force:true})
        cy.get('.swiper-button-prev').click({force:true})
        cy.get('.swiper-slide-active > .flex-column > .my-4').should('have.text','Past Rewards')
        cy.get('.swiper-slide-active > .flex-column > .text-white > :nth-child(1)').should('have.text','Baskin & Robbins Voucher')
        cy.get('.swiper-slide-active > .flex-column > .font-21 > :nth-child(1)').should('have.text','Expired')
        cy.get('.swiper-slide-active > .flex-column > .font-28 > :nth-child(1) > .d-flex').should('have.text','2000')
        cy.get('.swiper-slide-active > .flex-column > .text-white > :nth-child(2)').should('have.text','PVR Cinema Voucher')
        cy.get('.swiper-slide-active > .flex-column > .font-21 > :nth-child(2)').should('have.text','Expired')
        cy.get('.swiper-slide-active > .flex-column > .font-28 > :nth-child(2) > .d-flex').should('have.text','3000')
    })

    it('Future_Rewards', function()
    {
        cy.server()
        //cy.fixture('/home/homefixture_3.json').as('comment')
        cy.contains('Rewards').click({force:true})
        //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"7","name":"Baskin & Robbins Voucher","desc":"A voucher worth Rs. 350 to be spent at any Baskin & Robbins outlet on ice cream.","type":"Food","points":2000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":1,"__typename":"Reward"},{"id":"8","name":"PVR Cinema Voucher","desc":"A voucher worth Rs. 500 which can be availed at any PVR Cinema.","type":"Entertainment","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":2,"__typename":"Reward"}]}}).as('res1')
        cy.route('POST','/graphql',{"data":{"rewards":[{"id":"9","name":"Myntra Voucher","desc":"A voucher of Rs. 800 to be spent on any purchase on Myntra.com","type":"Shopping","points":5000,"time_to_arrive_in_ms":"1154433646","time_to_expire_in_ms":"16879233646","is_claimed":false,"level_type":3,"__typename":"Reward"}]}})
        cy.contains('Rewards').click({force:true})
        cy.get('.swiper-button-next > .img').click({force:true})
        cy.get('.swiper-slide-active > .flex-column > .my-4').should('have.text','Future Rewards')
        cy.get('.swiper-slide-active > .flex-column > .text-white > :nth-child(1)').should('have.text','Myntra Voucher')
        cy.get('.swiper-slide-active > .flex-column > .font-21 > :nth-child(1)').should('have.text','Arriving in 13 days')
        cy.get('.swiper-slide-active > .flex-column > .font-28 > :nth-child(1) > .d-flex').should('have.text','5000')
    })



    //for no rewards the behaviour is weird it shows blank page, is this the xpected behaviour.

    // it('No_Rewards', function()
    // {
    //     cy.server()
    //     cy.fixture('/home/homefixture_3.json').as('comment')
    //     cy.get('.m-auto > :nth-child(4) > .nav-link').click({force:true})
    //     //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"7","name":"Baskin & Robbins Voucher","desc":"A voucher worth Rs. 350 to be spent at any Baskin & Robbins outlet on ice cream.","type":"Food","points":2000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":1,"__typename":"Reward"},{"id":"8","name":"PVR Cinema Voucher","desc":"A voucher worth Rs. 500 which can be availed at any PVR Cinema.","type":"Entertainment","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":2,"__typename":"Reward"}]}}).as('res1')
    //     cy.route('POST','/graphql',{"data":{"rewards":[]}})
    //     //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"9","name":"Myntra Voucher","desc":"A voucher of Rs. 800 to be spent on any purchase on Myntra.com","type":"Shopping","points":5000,"time_to_arrive_in_ms":"1202234359","time_to_expire_in_ms":"16927034359","is_claimed":false,"level_type":3,"__typename":"Reward"}]}})
    //    cy.get(':nth-child(1) > .active').click({force:true})
    //     cy.get('.swiper-slide-active > .flex-column > .text-white > :nth-child(1)').should('have.text',"Domino's Voucher")
    //     cy.get('.swiper-slide-active > .flex-column > .font-21 > :nth-child(1)').should('have.text','52 days left')
    //     cy.get('.swiper-slide-active > .flex-column > .align-items-start.font-28 > :nth-child(1) > .d-flex').should('have.text','3000')
    //     cy.get('.btn-claim').should('have.text','Choose Reward').should('not.be.disabled')
        
    //     cy.get('.swiper-slide-active > .flex-column > .text-white > :nth-child(2)').should('have.text',"Amazon Gift Voucher")
    //     cy.get('.swiper-slide-active > .flex-column > .font-21 > :nth-child(2)').should('have.text','13 days left')
    //     cy.get('.swiper-slide-active > .flex-column > .align-items-start.font-28 > :nth-child(2) > .d-flex').should('have.text','5000')
    //     //cy.get('.btn-claim').should('have.text','Choose Reward').should('not.be.disabled')
    // })

    
    it('One_Rewards', function()
   {
        cy.server()
        cy.fixture('/home/homefixture_3.json').as('comment')
        cy.get('.m-auto > :nth-child(4) > .nav-link').click({force:true})
        //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"7","name":"Baskin & Robbins Voucher","desc":"A voucher worth Rs. 350 to be spent at any Baskin & Robbins outlet on ice cream.","type":"Food","points":2000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":1,"__typename":"Reward"},{"id":"8","name":"PVR Cinema Voucher","desc":"A voucher worth Rs. 500 which can be availed at any PVR Cinema.","type":"Entertainment","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":2,"__typename":"Reward"}]}}).as('res1')
        cy.route('POST','/graphql',{"data":{"rewards":[{"id":"5","name":"Domino's Voucher","desc":"A voucher which can be used to purchase Rs. 480 worth of food from any Domino's outlet via the Domino's App.","type":"Food","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"4571834407","is_claimed":false,"level_type":2,"__typename":"Reward"},{"id":"6","name":"Amazon Gift Voucher","desc":"Amazon Gift Voucher worth Rs. 800 which can be used to purchase anything from Amazon online.","type":"Shopping","points":5000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"1202234406","is_claimed":false,"level_type":3,"__typename":"Reward"}]}})
        //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"9","name":"Myntra Voucher","desc":"A voucher of Rs. 800 to be spent on any purchase on Myntra.com","type":"Shopping","points":5000,"time_to_arrive_in_ms":"1202234359","time_to_expire_in_ms":"16927034359","is_claimed":false,"level_type":3,"__typename":"Reward"}]}})
        cy.get(':nth-child(1) > .active').click({force:true})
        cy.get('.swiper-slide-active > .flex-column > .text-white > :nth-child(1)').should('have.text',"Domino's Voucher")
        cy.get('.swiper-slide-active > .flex-column > .font-21 > :nth-child(1)').should('have.text','52 days left')
        cy.get('.swiper-slide-active > .flex-column > .align-items-start.font-28 > :nth-child(1) > .d-flex').should('have.text','3000')
        cy.get('.btn-claim').should('have.text','Choose Reward').should('not.be.disabled')
        cy.get('.swiper-slide-active > .flex-column > .text-white > :nth-child(2)').should('have.text',"Amazon Gift Voucher")
        cy.get('.swiper-slide-active > .flex-column > .font-21 > :nth-child(2)').should('have.text','13 days left')
        cy.get('.swiper-slide-active > .flex-column > .align-items-start.font-28 > :nth-child(2) > .d-flex').should('have.text','5000')
        cy.get('.btn-claim').should('have.text','Choose Reward').should('not.be.disabled')
    })

    it('Claim_Reward', function()
    {
         cy.server()
         //cy.fixture('/home/homefixture_3.json').as('comment')
         cy.get('.m-auto > :nth-child(4) > .nav-link').click({force:true})
         //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"7","name":"Baskin & Robbins Voucher","desc":"A voucher worth Rs. 350 to be spent at any Baskin & Robbins outlet on ice cream.","type":"Food","points":2000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":1,"__typename":"Reward"},{"id":"8","name":"PVR Cinema Voucher","desc":"A voucher worth Rs. 500 which can be availed at any PVR Cinema.","type":"Entertainment","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":2,"__typename":"Reward"}]}}).as('res1')
         cy.route('POST','/graphql',{"data":{"rewards":[{"id":"5","name":"Domino's Voucher","desc":"A voucher which can be used to purchase Rs. 480 worth of food from any Domino's outlet via the Domino's App.","type":"Food","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"4571834407","is_claimed":false,"level_type":2,"__typename":"Reward"}]}})
         //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"9","name":"Myntra Voucher","desc":"A voucher of Rs. 800 to be spent on any purchase on Myntra.com","type":"Shopping","points":5000,"time_to_arrive_in_ms":"1202234359","time_to_expire_in_ms":"16927034359","is_claimed":false,"level_type":3,"__typename":"Reward"}]}})
         cy.get(':nth-child(1) > .active').click({force:true})
         cy.get('.swiper-slide-active > .flex-column > .text-white > :nth-child(1)').should('have.text',"Domino's Voucher")
         cy.get('.swiper-slide-active > .flex-column > .font-21 > :nth-child(1)').should('have.text','52 days left')
         cy.get('.swiper-slide-active > .flex-column > .align-items-start.font-28 > :nth-child(1) > .d-flex').should('have.text','3000')
         cy.get('.btn-claim').should('have.text','Choose Reward').should('not.be.disabled').click({force:true})
         cy.server()
         cy.route('POST','/graphql',{"data":{"student_goal_update":{"id":"5","points":3000,"name":"Domino's Voucher","level_type":2,"expires_in_ms":"4531862479","__typename":"CurrentGoal"}}}).as('response3')
         cy.contains('Select reward').click({force:true})
         cy.wait('@response3')
         cy.url().should('include','https://student-staging.lidolearning.com/')
         cy.server()
         //cy.fixture('/home/homefixture_3.json').as('comment')
         cy.get('.m-auto > :nth-child(4) > .nav-link').click({force:true})
         //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"7","name":"Baskin & Robbins Voucher","desc":"A voucher worth Rs. 350 to be spent at any Baskin & Robbins outlet on ice cream.","type":"Food","points":2000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":1,"__typename":"Reward"},{"id":"8","name":"PVR Cinema Voucher","desc":"A voucher worth Rs. 500 which can be availed at any PVR Cinema.","type":"Entertainment","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"0","is_claimed":false,"level_type":2,"__typename":"Reward"}]}}).as('res1')
         //do check the stubbing  here.
         cy.route('POST','/graphql',{"data":{"rewards":[{"id":"5","name":"Domino's Voucher","desc":"A voucher which can be used to purchase Rs. 480 worth of food from any Domino's outlet via the Domino's App.","type":"Food","points":3000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"4525935051","is_claimed":false,"level_type":2,"__typename":"Reward"},{"id":"6","name":"Amazon Gift Voucher","desc":"Amazon Gift Voucher worth Rs. 800 which can be used to purchase anything from Amazon online.","type":"Shopping","points":5000,"time_to_arrive_in_ms":"0","time_to_expire_in_ms":"1156335049","is_claimed":false,"level_type":3,"__typename":"Reward"}]}})
         //cy.route('POST','/graphql',{"data":{"rewards":[{"id":"9","name":"Myntra Voucher","desc":"A voucher of Rs. 800 to be spent on any purchase on Myntra.com","type":"Shopping","points":5000,"time_to_arrive_in_ms":"1202234359","time_to_expire_in_ms":"16927034359","is_claimed":false,"level_type":3,"__typename":"Reward"}]}})
         cy.get(':nth-child(1) > .active').click({force:true})
         cy.get('.swiper-slide-active > .flex-column > .text-white > :nth-child(1)').should('have.text',"Domino's Voucher")
         cy.get('.swiper-slide-active > .flex-column > .font-21 > :nth-child(1)').should('have.text','52 days left')
         cy.get('.swiper-slide-active > .flex-column > .align-items-start.font-28 > :nth-child(1) > .d-flex').should('have.text','3000')
         cy.get('.align-items-center.text-center > :nth-child(1) > .row > .progress').should('not.be.disabled')
         cy.contains('My Rewards').click({force:true})
         cy.contains('Total gems earned: 78')
         cy.get('.container > :nth-child(1) > .font-21').should('have.text','Working Towards')
         cy.get('.col-md-2').should('not.be.empty')
         cy.get('.font-24 > .d-flex').should('have.text','3000')
         cy.get('.col-md-7').should('not.be.empty')
         cy.get('.ml-3').should('have.text',"Domino's Voucher")
         cy.get('.col-md-3').should('not.be.empty')
         cy.get('.col-md-3 > .font-18').should('have.text','52 days')
     })
})


