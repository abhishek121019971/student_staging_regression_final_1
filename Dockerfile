FROM cypress/base
RUN mkdir cypo
ADD . /cypo
WORKDIR /cypo
RUN npm install --save-dev cypress
ARG Staging_Url
RUN echo "${Staging_Url}"
RUN CYPRESS_BASE_URL=${Staging_Url} node_modules/.bin/cypress run test; exit 0
